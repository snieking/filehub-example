const {
  FlagsType,
  Postchain,
  KeyPair,
  SingleSignatureAuthDescriptor,
  User,
} = require('ft3-lib');
const crypto = require('crypto');
const secp256k1 = require('secp256k1');

async function createFt3User(filehubUrl, filehubBrid, privKey) {
  const blockchain = await new Postchain(filehubUrl).blockchain(filehubBrid);

  const walletKeyPair = new KeyPair(
    privKey ? privKey : makeKeyPair().privKey.toString('hex')
  );
  const walletAuthDescriptor = new SingleSignatureAuthDescriptor(
    walletKeyPair.pubKey,
    [FlagsType.Account, FlagsType.Transfer]
  );

  const walletUser = new User(walletKeyPair, walletAuthDescriptor);

  try {
    await blockchain.registerAccount(walletAuthDescriptor, walletUser);
  } catch (error) {
    console.log('User already registered');
  }

  return new Promise((resolve) => resolve(walletUser));
}

function makeKeyPair() {
  let privKey;
  do {
    privKey = crypto.randomBytes(32);
  } while (!secp256k1.privateKeyVerify(privKey));
  const pubKey = secp256k1.publicKeyCreate(privKey);
  return { pubKey, privKey };
}

module.exports = {
  createFt3User,
};
