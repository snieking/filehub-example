const { Filehub, FsFile } = require('@snieking/fs-client');
const { createFt3User } = require('./util');

const fileToUpload = './test';

const filehubUrl =
  'https://gl90tpkjck.execute-api.eu-central-1.amazonaws.com/dev/';
const filehubBrid =
  '9C6BFD0B96803A131C1EED2ED773097E2DB5B4103B7CBD5CD23701D1F1D43C02';

async function main() {
  // This will create a new FT3 User each time,
  // in production this FT3 user will be charged for the storage
  // at the moment nothing is charged so any account will do
  const user = await createFt3User(filehubUrl, filehubBrid);

  const file = await FsFile.fromLocalFile(fileToUpload);

  const filehub = new Filehub(filehubUrl, filehubBrid);
  try {
    await filehub.storeFile(user, file);
  } catch (e) {
    console.log(e);
  }

  console.log('\n=============================================');
  console.log(`Filehub URL: ${filehubUrl}`);
  console.log(`Filehub BRID: ${filehubBrid}`);
  console.log(`File ID/Hash: ${file.hash.toString('hex')}`);
  console.log(`============================================='\n)`);
}

main();
